package com.example.notesapp

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.core.os.bundleOf
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.example.notesapp.fragments.HomeFragment
import com.example.notesapp.fragments.ViewNoteFragment
import com.example.notesapp.room.Note
import java.util.Date

class NotesAdapter(private val notes : List<Note>,
                   private val homeFragment : HomeFragment
) : RecyclerView.Adapter<NotesAdapter.ViewHolder>() {

    class ViewHolder(val view : View) : RecyclerView.ViewHolder(view) {
        val noteName: TextView = view.findViewById(R.id.note_name)
        val dateCreated: TextView = view.findViewById(R.id.date_created)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view  =
            LayoutInflater.from(parent.context).inflate(R.layout.note_holder, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.noteName.text = notes[position].noteName
        holder.dateCreated.text = Date(notes[position].date).toString()
        holder.view.setOnClickListener {
            val note = notes[holder.adapterPosition]
            bundleOf().apply {
                putString(ViewNoteFragment.NAME, note.noteName)
                putString(ViewNoteFragment.DESCRIPTION, note.noteText)
                putInt(ViewNoteFragment.ID, note.id)
                putLong(ViewNoteFragment.TIME_CREATED, note.date)
                homeFragment.findNavController().navigate(R.id.view_note_fragment, this)
            }
        }
    }

    override fun getItemCount() = notes.size
}