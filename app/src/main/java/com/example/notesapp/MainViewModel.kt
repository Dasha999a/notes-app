package com.example.notesapp

import android.content.Context
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.viewModelScope
import com.example.notesapp.room.Note
import com.example.notesapp.room.NoteRepository
import kotlinx.coroutines.launch

class MainViewModel(context: Context,
                    private val repository : NoteRepository = NoteRepository(context),
                    val notes : MutableLiveData<List<Note>> = MutableLiveData(),
) : ViewModel() {

    init {
        getAll()
    }

    fun insertNote(note : Note) {
        repository.insertNote(note)
        getAll()
    }

    private fun getAll() {
        viewModelScope.launch {
            notes.postValue(repository.getAll())
        }
    }

    fun update(note : Note) {
        repository.update(note)
    }

    fun delete(note : Note) {
        repository.delete(note)
    }
}

class ViewModelFactory(private val context : Context) : ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return MainViewModel(context) as T
    }
}