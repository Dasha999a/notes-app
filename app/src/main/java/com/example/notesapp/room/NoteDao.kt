package com.example.notesapp.room

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Update

@Dao
interface NoteDao {
    @Query("SELECT * FROM notes")
    fun getAll() : List<Note>

    @Insert
    fun insertNote(note: Note)

    @Update
    fun update(note: Note)

    @Delete
    fun delete(note: Note)
}