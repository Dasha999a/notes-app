package com.example.notesapp.room

import android.content.Context
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class NoteRepository(
    private val context : Context,
    private val db : NoteRoomDatabase = NoteRoomDatabase.getDatabase(context),
    private val noteDao: NoteDao = db.noteDao()
) {
    fun insertNote(note : Note) {
        CoroutineScope(Dispatchers.IO).launch { noteDao.insertNote(note) }
    }
    suspend fun getAll(): List<Note> {
        return withContext(Dispatchers.IO) {
            noteDao.getAll()
        }
    }

    fun update(note : Note) {
        CoroutineScope(Dispatchers.IO).launch { noteDao.update(note) }
    }

    fun delete(note : Note) {
        CoroutineScope(Dispatchers.IO).launch { noteDao.delete(note) }
    }
}