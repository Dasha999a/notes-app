package com.example.notesapp.room

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase

@Database(entities = [Note::class], version = 2, exportSchema = false)
abstract class NoteRoomDatabase : RoomDatabase() {
    companion object {
        private var instance : NoteRoomDatabase? = null

        fun getDatabase(context : Context) : NoteRoomDatabase {
            return instance ?: synchronized(this) {
                instance ?: Room.databaseBuilder(context, NoteRoomDatabase::class.java, "notes")
                    .fallbackToDestructiveMigration()
                    .build().also { instance = it }
            }
        }
    }

    abstract fun noteDao() : NoteDao
}