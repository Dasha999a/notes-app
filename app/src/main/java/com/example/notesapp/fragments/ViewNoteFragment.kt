package com.example.notesapp.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.core.os.bundleOf
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.example.notesapp.MainViewModel
import com.example.notesapp.R
import com.example.notesapp.ViewModelFactory
import com.example.notesapp.databinding.FragmentViewNoteBinding
import com.example.notesapp.room.Note
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.analytics.logEvent

class ViewNoteFragment : Fragment() {
    private lateinit var binding : FragmentViewNoteBinding
    private lateinit var firebaseAnalytics: FirebaseAnalytics
    companion object {
        const val NAME = "name"
        const val DESCRIPTION = "description"
        const val ID = "id"
        const val TIME_CREATED = "time_created"
    }
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentViewNoteBinding.inflate(layoutInflater)
        firebaseAnalytics = FirebaseAnalytics.getInstance(requireContext())
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val factory = ViewModelFactory(requireContext())
        val viewModel : MainViewModel =
            ViewModelProvider(this, factory)[MainViewModel::class.java]

        (activity as AppCompatActivity).supportActionBar?.title =
            getString(R.string.view_note_title)

        val name = arguments?.getString(NAME)
        val description = arguments?.getString(DESCRIPTION)
        val id = arguments?.getInt(ID)
        val timeCreated = arguments?.getLong(TIME_CREATED)

        firebaseAnalytics.logEvent("view_note") {
            name?.let { param("note_name", it) }
            description?.let { param("note_text", it) }
        }

        binding.noteName.text = name
        binding.noteDescription.text = description

        val oldNote = Note(id!!, name!!, description!!, timeCreated!!)

        binding.deleteButton.setOnClickListener {
            viewModel.delete(oldNote)
            findNavController().navigate(R.id.home_fragment)

            firebaseAnalytics.logEvent("remove_note") {
                param("note_name", name)
                param("note_text", description)
            }
        }

        binding.editButton.setOnClickListener {
            bundleOf().apply {
                putString(NAME, name)
                putString(DESCRIPTION, description)
                putInt(ID, id)
                findNavController().navigate(R.id.edit_note_fragment, this)
            }
        }
    }
}