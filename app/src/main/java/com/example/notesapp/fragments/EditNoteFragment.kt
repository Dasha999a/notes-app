package com.example.notesapp.fragments

import android.os.Bundle
import android.text.Editable
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.example.notesapp.MainViewModel
import com.example.notesapp.R
import com.example.notesapp.ViewModelFactory
import com.example.notesapp.databinding.FragmentEditNoteBinding
import com.example.notesapp.room.Note

class EditNoteFragment : Fragment() {
    private lateinit var binding : FragmentEditNoteBinding
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentEditNoteBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val factory = ViewModelFactory(requireContext())
        val viewModel : MainViewModel =
            ViewModelProvider(this, factory)[MainViewModel::class.java]

        (activity as AppCompatActivity).supportActionBar?.title =
            getString(R.string.edit_note_title)

        val name = arguments?.getString(ViewNoteFragment.NAME)
        val id = arguments?.getInt(ViewNoteFragment.ID)
        val time = arguments?.getLong(ViewNoteFragment.TIME_CREATED)

        binding.nameEditText.text =
            Editable.Factory.getInstance()
                .newEditable(name)
        binding.nameEditText.isEnabled = false
        binding.noteEditText.text =
            Editable.Factory.getInstance()
                .newEditable(arguments?.getString(ViewNoteFragment.DESCRIPTION))

        binding.saveButton.setOnClickListener {
            val note = Note(id!!, name!!, binding.noteEditText.text.toString(), time!!)
            viewModel.update(note)
            findNavController().navigate(R.id.home_fragment)
        }
    }
}