package com.example.notesapp.fragments

import android.os.Build
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.example.notesapp.MainViewModel
import com.example.notesapp.R
import com.example.notesapp.ViewModelFactory
import com.example.notesapp.databinding.FragmentAddNoteBinding
import com.example.notesapp.room.Note
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.analytics.logEvent

class AddNoteFragment : Fragment() {
    private lateinit var binding : FragmentAddNoteBinding
    private lateinit var firebaseAnalytics: FirebaseAnalytics
    @RequiresApi(Build.VERSION_CODES.O)
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentAddNoteBinding.inflate(layoutInflater)
        firebaseAnalytics = FirebaseAnalytics.getInstance(requireContext())

        val factory = ViewModelFactory(requireContext())
        val viewModel : MainViewModel =
            ViewModelProvider(this, factory)[MainViewModel::class.java]

        (activity as AppCompatActivity).supportActionBar?.title =
            getString(R.string.add_note_title)

        binding.saveButton.setOnClickListener {
            val noteName = binding.nameEditText.text.toString()
            val noteText = binding.noteEditText.text.toString()
            val note =
                Note(noteName = noteName, noteText = noteText, date = System.currentTimeMillis())
            viewModel.insertNote(note)

            firebaseAnalytics.logEvent("add_note") {
                param("note_name", noteName)
                param("note_text", noteText)
            }

            findNavController().navigate(R.id.home_fragment)
        }

        return binding.root
    }
}