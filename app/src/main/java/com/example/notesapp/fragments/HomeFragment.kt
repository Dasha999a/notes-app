package com.example.notesapp.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.notesapp.MainViewModel
import com.example.notesapp.NotesAdapter
import com.example.notesapp.R
import com.example.notesapp.ViewModelFactory
import com.example.notesapp.databinding.FragmentHomeBinding
import com.example.notesapp.room.Note

class HomeFragment : Fragment(){
    private lateinit var binding : FragmentHomeBinding
    private lateinit var viewModel : MainViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentHomeBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val factory = ViewModelFactory(requireContext())
        viewModel =
            ViewModelProvider(this, factory)[MainViewModel::class.java]

        val notes : MutableList<Note> = mutableListOf()
        val adapter = NotesAdapter(notes, this)
        binding.notes.adapter = adapter
        binding.notes.layoutManager = LinearLayoutManager(requireContext())

        viewModel.notes.observe(viewLifecycleOwner) {
            notes.clear()
            notes.addAll(it)
            adapter.notifyDataSetChanged()
        }


        binding.createNoteButton.setOnClickListener {
            findNavController().navigate(R.id.add_note_fragment)
        }
    }
}